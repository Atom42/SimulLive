package com.reysinc.simullive;

import android.animation.*;
import android.app.*;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.*;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.media.*;
import android.net.*;
import android.net.Uri;
import android.os.*;
import android.text.*;
import android.text.style.*;
import android.util.*;
import android.view.*;
import android.view.View;
import android.view.View.*;
import android.view.animation.*;
import android.webkit.*;
import android.widget.*;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;
import org.json.*;

public class MainActivity extends Activity {
	
	private ScrollView vscroll1;
	private LinearLayout linear1;
	private TextView textview1;
	private Button button1;
	private Button button2;
	private Button button3;
	private LinearLayout linear2;
	private Button button6;
	private Button button7;
	private Button button4;
	private Button button5;
	
	private Intent q = new Intent();
	private AlertDialog.Builder d;
	private SharedPreferences filegame;
	private SharedPreferences filemoney;
	private AlertDialog.Builder dg;
	private AlertDialog.Builder bug;
	private SharedPreferences frees;
	private SharedPreferences sred;
	private SharedPreferences full;
	private SharedPreferences dvor;
	private SharedPreferences rab;
	private SharedPreferences prod;
	private SharedPreferences parik;
	private SharedPreferences XP;
	private SharedPreferences Seven;
	private SharedPreferences Android;
	private SharedPreferences ten;
	private SharedPreferences prog;
	private SharedPreferences progdoma;
	private SharedPreferences kurs;
	private SharedPreferences slowswitz;
	private Intent w = new Intent();
	private SharedPreferences agf;
	
	@Override
	protected void onCreate(Bundle _savedInstanceState) {
		super.onCreate(_savedInstanceState);
		setContentView(R.layout.main);
		initialize(_savedInstanceState);
		initializeLogic();
	}
	
	private void initialize(Bundle _savedInstanceState) {
		vscroll1 = findViewById(R.id.vscroll1);
		linear1 = findViewById(R.id.linear1);
		textview1 = findViewById(R.id.textview1);
		button1 = findViewById(R.id.button1);
		button2 = findViewById(R.id.button2);
		button3 = findViewById(R.id.button3);
		linear2 = findViewById(R.id.linear2);
		button6 = findViewById(R.id.button6);
		button7 = findViewById(R.id.button7);
		button4 = findViewById(R.id.button4);
		button5 = findViewById(R.id.button5);
		d = new AlertDialog.Builder(this);
		filegame = getSharedPreferences("filegame", Activity.MODE_PRIVATE);
		filemoney = getSharedPreferences("filemoney", Activity.MODE_PRIVATE);
		dg = new AlertDialog.Builder(this);
		bug = new AlertDialog.Builder(this);
		frees = getSharedPreferences("frees", Activity.MODE_PRIVATE);
		sred = getSharedPreferences("sred", Activity.MODE_PRIVATE);
		full = getSharedPreferences("full", Activity.MODE_PRIVATE);
		dvor = getSharedPreferences("dvor", Activity.MODE_PRIVATE);
		rab = getSharedPreferences("rab", Activity.MODE_PRIVATE);
		prod = getSharedPreferences("prod", Activity.MODE_PRIVATE);
		parik = getSharedPreferences("parik", Activity.MODE_PRIVATE);
		XP = getSharedPreferences("XP", Activity.MODE_PRIVATE);
		Seven = getSharedPreferences("Seven", Activity.MODE_PRIVATE);
		Android = getSharedPreferences("Android", Activity.MODE_PRIVATE);
		ten = getSharedPreferences("ten", Activity.MODE_PRIVATE);
		prog = getSharedPreferences("prog", Activity.MODE_PRIVATE);
		progdoma = getSharedPreferences("progdoma", Activity.MODE_PRIVATE);
		kurs = getSharedPreferences("kurs", Activity.MODE_PRIVATE);
		slowswitz = getSharedPreferences("slowswitz", Activity.MODE_PRIVATE);
		agf = getSharedPreferences("agf", Activity.MODE_PRIVATE);
		
		button1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				dg.setTitle("Игра");
				dg.setMessage("Вы хотите начать новую игру?");
				dg.setPositiveButton("Да", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface _dialog, int _which) {
						q.setClass(getApplicationContext(), GameActivity.class);
						startActivity(q);
						filegame.edit().putString("new", "0").commit();
						frees.edit().putString("freee", "0").commit();
						sred.edit().putString("normal", "0").commit();
						full.edit().putString("fuller", "0").commit();
						dvor.edit().putString("dvor", "0").commit();
						rab.edit().putString("rab", "0").commit();
						prod.edit().putString("prod", "0").commit();
						parik.edit().putString("parik", "0").commit();
						XP.edit().putString("XP", "0").commit();
						Seven.edit().putString("Seven", "0").commit();
						Android.edit().putString("Android", "0").commit();
						ten.edit().putString("ten", "0").commit();
						progdoma.edit().putString("progdoma", "0").commit();
						prog.edit().putString("prog", "0").commit();
						kurs.edit().putString("kurs", "0").commit();
						slowswitz.edit().putString("ss", "1000").commit();
						agf.edit().putString("school", "0").commit();
						agf.edit().putString("sschool", "0").commit();
						agf.edit().putString("ssschool", "0").commit();
						agf.edit().putString("eges", "0").commit();
						agf.edit().putString("do", "0").commit();
						agf.edit().putString("uch", "0").commit();
						agf.edit().putString("sl", "0").commit();
						agf.edit().putString("ssl", "0").commit();
						agf.edit().putString("sssl", "0").commit();
						finish();
					}
				});
				dg.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface _dialog, int _which) {
						
					}
				});
				dg.create().show();
			}
		});
		
		button2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				q.setClass(getApplicationContext(), GameActivity.class);
				startActivity(q);
				filegame.edit().putString("new", "1").commit();
				finish();
			}
		});
		
		button3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				q.setClass(getApplicationContext(), InfoActivity.class);
				startActivity(q);
			}
		});
		
		button6.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				q.setClass(getApplicationContext(), ConsoleActivity.class);
				startActivity(q);
			}
		});
		
		button7.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				w.setAction(Intent.ACTION_VIEW);
				w.setData(Uri.parse("https://play.google.com/store/apps/details?id=sibcraftgames.simullivelifesimulator"));
				startActivity(w);
			}
		});
		
		button4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				w.setAction(Intent.ACTION_VIEW);
				w.setData(Uri.parse("https://play.google.com/store/apps/dev?id=5139686623617778921"));
				startActivity(w);
			}
		});
		
		button5.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				d.setTitle("Выход");
				d.setMessage("Вы хотите выйти из игры?");
				d.setPositiveButton("Да", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface _dialog, int _which) {
						finish();
					}
				});
				d.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface _dialog, int _which) {
						
					}
				});
				d.create().show();
			}
		});
	}
	
	private void initializeLogic() {
		button6.setVisibility(View.GONE);
		if (filegame.getString("new", "").equals("")) {
			button2.setVisibility(View.GONE);
		}
		else {
			button2.setVisibility(View.VISIBLE);
		}
		Intent i = getPackageManager().getLaunchIntentForPackage("com.reysinc.developermode"); if (i == null) { return; } button6.setVisibility(View.VISIBLE);
	}
	
	@Override
	public void onBackPressed() {
		d.setTitle("Выход");
		d.setMessage("Вы хотите выйти из игры?");
		d.setPositiveButton("Да", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface _dialog, int _which) {
				finish();
			}
		});
		d.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface _dialog, int _which) {
				
			}
		});
		d.create().show();
	}
	
	@Deprecated
	public void showMessage(String _s) {
		Toast.makeText(getApplicationContext(), _s, Toast.LENGTH_SHORT).show();
	}
	
	@Deprecated
	public int getLocationX(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[0];
	}
	
	@Deprecated
	public int getLocationY(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[1];
	}
	
	@Deprecated
	public int getRandom(int _min, int _max) {
		Random random = new Random();
		return random.nextInt(_max - _min + 1) + _min;
	}
	
	@Deprecated
	public ArrayList<Double> getCheckedItemPositionsToArray(ListView _list) {
		ArrayList<Double> _result = new ArrayList<Double>();
		SparseBooleanArray _arr = _list.getCheckedItemPositions();
		for (int _iIdx = 0; _iIdx < _arr.size(); _iIdx++) {
			if (_arr.valueAt(_iIdx))
			_result.add((double)_arr.keyAt(_iIdx));
		}
		return _result;
	}
	
	@Deprecated
	public float getDip(int _input) {
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, _input, getResources().getDisplayMetrics());
	}
	
	@Deprecated
	public int getDisplayWidthPixels() {
		return getResources().getDisplayMetrics().widthPixels;
	}
	
	@Deprecated
	public int getDisplayHeightPixels() {
		return getResources().getDisplayMetrics().heightPixels;
	}
}