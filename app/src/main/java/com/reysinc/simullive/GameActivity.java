package com.reysinc.simullive;

import android.animation.*;
import android.app.*;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.*;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.media.*;
import android.net.*;
import android.net.Uri;
import android.os.*;
import android.text.*;
import android.text.style.*;
import android.util.*;
import android.view.*;
import android.view.View;
import android.view.View.*;
import android.view.animation.*;
import android.webkit.*;
import android.widget.*;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.io.*;
import java.text.*;
import java.util.*;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.*;
import org.json.*;

public class GameActivity extends Activity {
	
	private Timer _timer = new Timer();
	
	private double swmn1 = 0;
	private double swmn2 = 0;
	
	private HorizontalScrollView hscroll1;
	private ScrollView vscroll1;
	private LinearLayout linear1;
	private TextView textview1;
	private TextView textview2;
	private TextView textview3;
	private TextView golod;
	private TextView golod0;
	private TextView swither;
	private TextView swither0;
	private TextView textview4;
	private LinearLayout linear2;
	private HorizontalScrollView hscroll5;
	private LinearLayout rabota;
	private LinearLayout doprabota;
	private LinearLayout zrachka;
	private LinearLayout house;
	private LinearLayout naslodi;
	private LinearLayout os;
	private LinearLayout dop;
	private LinearLayout mechtafuture;
	private LinearLayout linear3;
	private TextView rabstvo;
	private TextView doprabstvo;
	private TextView foodzrachka;
	private TextView noslozdenia;
	private TextView hhoommee;
	private TextView advanced;
	private TextView opersys;
	private TextView future;
	private Button button3;
	private Button button4;
	private Button button17;
	private Button button32;
	private Button button36;
	private Button button35;
	private Button button2;
	private Button button27;
	private Button button28;
	private Button button42;
	private Button button43;
	private Button button5;
	private Button button6;
	private Button button7;
	private Button button14;
	private Button button15;
	private Button button45;
	private Button button29;
	private Button button30;
	private Button button31;
	private Button button9;
	private Button button59;
	private Button button58;
	private Button button46;
	private Button button47;
	private Button button10;
	private Button button12;
	private Button button13;
	private Button button40;
	private Button button37;
	private Button button38;
	private Button button39;
	private LinearLayout linear4;
	private LinearLayout linear5;
	private Button uchvschool;
	private Button button50;
	private Button button51;
	private Button button53;
	private Button button48;
	private TextView textview5;
	private TextView ege;
	private TextView textview7;
	private Button button52;
	private TextView textview8;
	private TextView textview9;
	private TextView textview10;
	private TextView thisneedtodelete;
	private Button button54;
	private Button button34;
	private Button button33;
	private Button button55;
	private Button button44;
	private Button button56;
	
	private Intent q = new Intent();
	private SharedPreferences filemoney;
	private SharedPreferences filegame;
	private AlertDialog.Builder d;
	private SharedPreferences eda;
	private SharedPreferences swith;
	private TimerTask goloden;
	private TimerTask snf;
	private TimerTask proverka;
	private TimerTask dvornik;
	private TimerTask rabochiy;
	private TimerTask parick;
	private TimerTask musor;
	private TimerTask goparick;
	private TimerTask free;
	private TimerTask norm;
	private TimerTask prof;
	private SharedPreferences frees;
	private SharedPreferences sred;
	private SharedPreferences full;
	private TimerTask prodavec;
	private SharedPreferences dvor;
	private SharedPreferences rab;
	private SharedPreferences prod;
	private SharedPreferences parik;
	private SharedPreferences XP;
	private SharedPreferences Seven;
	private SharedPreferences Android;
	private SharedPreferences ten;
	private SharedPreferences die;
	private SharedPreferences win;
	private SharedPreferences progdoma;
	private SharedPreferences prog;
	private TimerTask progdomas;
	private TimerTask programmer;
	private TimerTask andro;
	private SharedPreferences kurs;
	private TimerTask perw;
	private TimerTask cuapp;
	private SharedPreferences slowswitz;
	private TimerTask ss;
	private AlertDialog.Builder PochtiEmpty;
	private SharedPreferences allgamefile;
	private TimerTask s1;
	private TimerTask ss1;
	
	@Override
	protected void onCreate(Bundle _savedInstanceState) {
		super.onCreate(_savedInstanceState);
		setContentView(R.layout.game);
		initialize(_savedInstanceState);
		initializeLogic();
	}
	
	private void initialize(Bundle _savedInstanceState) {
		hscroll1 = findViewById(R.id.hscroll1);
		vscroll1 = findViewById(R.id.vscroll1);
		linear1 = findViewById(R.id.linear1);
		textview1 = findViewById(R.id.textview1);
		textview2 = findViewById(R.id.textview2);
		textview3 = findViewById(R.id.textview3);
		golod = findViewById(R.id.golod);
		golod0 = findViewById(R.id.golod0);
		swither = findViewById(R.id.swither);
		swither0 = findViewById(R.id.swither0);
		textview4 = findViewById(R.id.textview4);
		linear2 = findViewById(R.id.linear2);
		hscroll5 = findViewById(R.id.hscroll5);
		rabota = findViewById(R.id.rabota);
		doprabota = findViewById(R.id.doprabota);
		zrachka = findViewById(R.id.zrachka);
		house = findViewById(R.id.house);
		naslodi = findViewById(R.id.naslodi);
		os = findViewById(R.id.os);
		dop = findViewById(R.id.dop);
		mechtafuture = findViewById(R.id.mechtafuture);
		linear3 = findViewById(R.id.linear3);
		rabstvo = findViewById(R.id.rabstvo);
		doprabstvo = findViewById(R.id.doprabstvo);
		foodzrachka = findViewById(R.id.foodzrachka);
		noslozdenia = findViewById(R.id.noslozdenia);
		hhoommee = findViewById(R.id.hhoommee);
		advanced = findViewById(R.id.advanced);
		opersys = findViewById(R.id.opersys);
		future = findViewById(R.id.future);
		button3 = findViewById(R.id.button3);
		button4 = findViewById(R.id.button4);
		button17 = findViewById(R.id.button17);
		button32 = findViewById(R.id.button32);
		button36 = findViewById(R.id.button36);
		button35 = findViewById(R.id.button35);
		button2 = findViewById(R.id.button2);
		button27 = findViewById(R.id.button27);
		button28 = findViewById(R.id.button28);
		button42 = findViewById(R.id.button42);
		button43 = findViewById(R.id.button43);
		button5 = findViewById(R.id.button5);
		button6 = findViewById(R.id.button6);
		button7 = findViewById(R.id.button7);
		button14 = findViewById(R.id.button14);
		button15 = findViewById(R.id.button15);
		button45 = findViewById(R.id.button45);
		button29 = findViewById(R.id.button29);
		button30 = findViewById(R.id.button30);
		button31 = findViewById(R.id.button31);
		button9 = findViewById(R.id.button9);
		button59 = findViewById(R.id.button59);
		button58 = findViewById(R.id.button58);
		button46 = findViewById(R.id.button46);
		button47 = findViewById(R.id.button47);
		button10 = findViewById(R.id.button10);
		button12 = findViewById(R.id.button12);
		button13 = findViewById(R.id.button13);
		button40 = findViewById(R.id.button40);
		button37 = findViewById(R.id.button37);
		button38 = findViewById(R.id.button38);
		button39 = findViewById(R.id.button39);
		linear4 = findViewById(R.id.linear4);
		linear5 = findViewById(R.id.linear5);
		uchvschool = findViewById(R.id.uchvschool);
		button50 = findViewById(R.id.button50);
		button51 = findViewById(R.id.button51);
		button53 = findViewById(R.id.button53);
		button48 = findViewById(R.id.button48);
		textview5 = findViewById(R.id.textview5);
		ege = findViewById(R.id.ege);
		textview7 = findViewById(R.id.textview7);
		button52 = findViewById(R.id.button52);
		textview8 = findViewById(R.id.textview8);
		textview9 = findViewById(R.id.textview9);
		textview10 = findViewById(R.id.textview10);
		thisneedtodelete = findViewById(R.id.thisneedtodelete);
		button54 = findViewById(R.id.button54);
		button34 = findViewById(R.id.button34);
		button33 = findViewById(R.id.button33);
		button55 = findViewById(R.id.button55);
		button44 = findViewById(R.id.button44);
		button56 = findViewById(R.id.button56);
		filemoney = getSharedPreferences("filemoney", Activity.MODE_PRIVATE);
		filegame = getSharedPreferences("filegame", Activity.MODE_PRIVATE);
		d = new AlertDialog.Builder(this);
		eda = getSharedPreferences("eda", Activity.MODE_PRIVATE);
		swith = getSharedPreferences("swith", Activity.MODE_PRIVATE);
		frees = getSharedPreferences("frees", Activity.MODE_PRIVATE);
		sred = getSharedPreferences("sred", Activity.MODE_PRIVATE);
		full = getSharedPreferences("full", Activity.MODE_PRIVATE);
		dvor = getSharedPreferences("dvor", Activity.MODE_PRIVATE);
		rab = getSharedPreferences("rab", Activity.MODE_PRIVATE);
		prod = getSharedPreferences("prod", Activity.MODE_PRIVATE);
		parik = getSharedPreferences("parik", Activity.MODE_PRIVATE);
		XP = getSharedPreferences("XP", Activity.MODE_PRIVATE);
		Seven = getSharedPreferences("Seven", Activity.MODE_PRIVATE);
		Android = getSharedPreferences("Android", Activity.MODE_PRIVATE);
		ten = getSharedPreferences("ten", Activity.MODE_PRIVATE);
		die = getSharedPreferences("die", Activity.MODE_PRIVATE);
		win = getSharedPreferences("win", Activity.MODE_PRIVATE);
		progdoma = getSharedPreferences("progdoma", Activity.MODE_PRIVATE);
		prog = getSharedPreferences("prog", Activity.MODE_PRIVATE);
		kurs = getSharedPreferences("kurs", Activity.MODE_PRIVATE);
		slowswitz = getSharedPreferences("slowswitz", Activity.MODE_PRIVATE);
		PochtiEmpty = new AlertDialog.Builder(this);
		allgamefile = getSharedPreferences("agf", Activity.MODE_PRIVATE);
		
		rabstvo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				rabota.setVisibility(View.VISIBLE);
				doprabota.setVisibility(View.GONE);
				zrachka.setVisibility(View.GONE);
				naslodi.setVisibility(View.GONE);
				house.setVisibility(View.GONE);
				os.setVisibility(View.GONE);
				dop.setVisibility(View.GONE);
				mechtafuture.setVisibility(View.GONE);
				rabstvo.setBackgroundColor(0xFF009688);
				doprabstvo.setBackgroundColor(0xFF9E9E9E);
				foodzrachka.setBackgroundColor(0xFF9E9E9E);
				noslozdenia.setBackgroundColor(0xFF9E9E9E);
				hhoommee.setBackgroundColor(0xFF9E9E9E);
				opersys.setBackgroundColor(0xFF9E9E9E);
				advanced.setBackgroundColor(0xFF9E9E9E);
				future.setBackgroundColor(0xFF9E9E9E);
			}
		});
		
		doprabstvo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				rabota.setVisibility(View.GONE);
				doprabota.setVisibility(View.VISIBLE);
				zrachka.setVisibility(View.GONE);
				naslodi.setVisibility(View.GONE);
				house.setVisibility(View.GONE);
				os.setVisibility(View.GONE);
				dop.setVisibility(View.GONE);
				mechtafuture.setVisibility(View.GONE);
				rabstvo.setBackgroundColor(0xFF9E9E9E);
				doprabstvo.setBackgroundColor(0xFF009688);
				foodzrachka.setBackgroundColor(0xFF9E9E9E);
				noslozdenia.setBackgroundColor(0xFF9E9E9E);
				hhoommee.setBackgroundColor(0xFF9E9E9E);
				opersys.setBackgroundColor(0xFF9E9E9E);
				advanced.setBackgroundColor(0xFF9E9E9E);
				future.setBackgroundColor(0xFF9E9E9E);
			}
		});
		
		foodzrachka.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				rabota.setVisibility(View.GONE);
				doprabota.setVisibility(View.GONE);
				zrachka.setVisibility(View.VISIBLE);
				naslodi.setVisibility(View.GONE);
				house.setVisibility(View.GONE);
				os.setVisibility(View.GONE);
				dop.setVisibility(View.GONE);
				mechtafuture.setVisibility(View.GONE);
				rabstvo.setBackgroundColor(0xFF9E9E9E);
				doprabstvo.setBackgroundColor(0xFF9E9E9E);
				foodzrachka.setBackgroundColor(0xFF009688);
				noslozdenia.setBackgroundColor(0xFF9E9E9E);
				hhoommee.setBackgroundColor(0xFF9E9E9E);
				opersys.setBackgroundColor(0xFF9E9E9E);
				advanced.setBackgroundColor(0xFF9E9E9E);
				future.setBackgroundColor(0xFF9E9E9E);
			}
		});
		
		noslozdenia.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				rabota.setVisibility(View.GONE);
				doprabota.setVisibility(View.GONE);
				zrachka.setVisibility(View.GONE);
				naslodi.setVisibility(View.VISIBLE);
				house.setVisibility(View.GONE);
				os.setVisibility(View.GONE);
				dop.setVisibility(View.GONE);
				mechtafuture.setVisibility(View.GONE);
				rabstvo.setBackgroundColor(0xFF9E9E9E);
				doprabstvo.setBackgroundColor(0xFF9E9E9E);
				foodzrachka.setBackgroundColor(0xFF9E9E9E);
				noslozdenia.setBackgroundColor(0xFF009688);
				hhoommee.setBackgroundColor(0xFF9E9E9E);
				opersys.setBackgroundColor(0xFF9E9E9E);
				advanced.setBackgroundColor(0xFF9E9E9E);
				future.setBackgroundColor(0xFF9E9E9E);
			}
		});
		
		hhoommee.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				rabota.setVisibility(View.GONE);
				doprabota.setVisibility(View.GONE);
				zrachka.setVisibility(View.GONE);
				naslodi.setVisibility(View.GONE);
				house.setVisibility(View.VISIBLE);
				os.setVisibility(View.GONE);
				dop.setVisibility(View.GONE);
				mechtafuture.setVisibility(View.GONE);
				rabstvo.setBackgroundColor(0xFF9E9E9E);
				doprabstvo.setBackgroundColor(0xFF9E9E9E);
				foodzrachka.setBackgroundColor(0xFF9E9E9E);
				noslozdenia.setBackgroundColor(0xFF9E9E9E);
				hhoommee.setBackgroundColor(0xFF009688);
				opersys.setBackgroundColor(0xFF9E9E9E);
				advanced.setBackgroundColor(0xFF9E9E9E);
				future.setBackgroundColor(0xFF9E9E9E);
			}
		});
		
		advanced.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				rabota.setVisibility(View.GONE);
				doprabota.setVisibility(View.GONE);
				zrachka.setVisibility(View.GONE);
				naslodi.setVisibility(View.GONE);
				house.setVisibility(View.GONE);
				os.setVisibility(View.GONE);
				dop.setVisibility(View.VISIBLE);
				mechtafuture.setVisibility(View.GONE);
				rabstvo.setBackgroundColor(0xFF9E9E9E);
				doprabstvo.setBackgroundColor(0xFF9E9E9E);
				foodzrachka.setBackgroundColor(0xFF9E9E9E);
				noslozdenia.setBackgroundColor(0xFF9E9E9E);
				hhoommee.setBackgroundColor(0xFF9E9E9E);
				opersys.setBackgroundColor(0xFF9E9E9E);
				advanced.setBackgroundColor(0xFF009688);
				future.setBackgroundColor(0xFF9E9E9E);
			}
		});
		
		opersys.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				rabota.setVisibility(View.GONE);
				doprabota.setVisibility(View.GONE);
				zrachka.setVisibility(View.GONE);
				naslodi.setVisibility(View.GONE);
				house.setVisibility(View.GONE);
				os.setVisibility(View.VISIBLE);
				dop.setVisibility(View.GONE);
				mechtafuture.setVisibility(View.GONE);
				rabstvo.setBackgroundColor(0xFF9E9E9E);
				doprabstvo.setBackgroundColor(0xFF9E9E9E);
				foodzrachka.setBackgroundColor(0xFF9E9E9E);
				noslozdenia.setBackgroundColor(0xFF9E9E9E);
				hhoommee.setBackgroundColor(0xFF9E9E9E);
				opersys.setBackgroundColor(0xFF009688);
				advanced.setBackgroundColor(0xFF9E9E9E);
				future.setBackgroundColor(0xFF9E9E9E);
			}
		});
		
		future.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				rabota.setVisibility(View.GONE);
				doprabota.setVisibility(View.GONE);
				zrachka.setVisibility(View.GONE);
				naslodi.setVisibility(View.GONE);
				house.setVisibility(View.GONE);
				os.setVisibility(View.GONE);
				dop.setVisibility(View.GONE);
				mechtafuture.setVisibility(View.VISIBLE);
				rabstvo.setBackgroundColor(0xFF9E9E9E);
				doprabstvo.setBackgroundColor(0xFF9E9E9E);
				foodzrachka.setBackgroundColor(0xFF9E9E9E);
				noslozdenia.setBackgroundColor(0xFF9E9E9E);
				hhoommee.setBackgroundColor(0xFF9E9E9E);
				opersys.setBackgroundColor(0xFF9E9E9E);
				advanced.setBackgroundColor(0xFF9E9E9E);
				future.setBackgroundColor(0xFF009688);
			}
		});
		
		button3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (allgamefile.getString("school", "").equals("1")) {
					dvornik = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + 10)));
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(dvornik, (int)(1000), (int)(1000));
					button3.setEnabled(false);
					dvor.edit().putString("dvor", "1").commit();
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Вы не можете устроиться на эту работу!");
				}
			}
		});
		
		button4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (allgamefile.getString("sschool", "").equals("1")) {
					rabochiy = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + 800)));
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(rabochiy, (int)(30000), (int)(30000));
					button4.setEnabled(false);
					rab.edit().putString("rab", "1").commit();
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Вы не можете устроиться на эту работу!");
				}
			}
		});
		
		button17.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (allgamefile.getString("ssschool", "").equals("1")) {
					prodavec = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + 2000)));
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(prodavec, (int)(30000), (int)(30000));
					button17.setEnabled(false);
					prod.edit().putString("prod", "1").commit();
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Вы не можете устроиться на эту работу!");
				}
			}
		});
		
		button32.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (allgamefile.getString("do", "").equals("1")) {
					parick = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + 5000)));
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(parick, (int)(30000), (int)(30000));
					button32.setEnabled(false);
					parik.edit().putString("parik", "1").commit();
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Вы не можете устроиться на эту работу!");
				}
			}
		});
		
		button36.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(kurs.getString("kurs", "")) == 1) {
					if (((Double.parseDouble(XP.getString("XP", "")) == 1) || (Double.parseDouble(Seven.getString("Seven", "")) == 1)) || (Double.parseDouble(ten.getString("ten", "")) == 1)) {
						button36.setEnabled(false);
						progdoma.edit().putString("progdoma", "1").commit();
						progdomas = new TimerTask() {
							@Override
							public void run() {
								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + SketchwareUtil.getRandom((int)(0), (int)(5500)))));
									}
								});
							}
						};
						_timer.scheduleAtFixedRate(progdomas, (int)(30000), (int)(30000));
					}
					else {
						SketchwareUtil.showMessage(getApplicationContext(), "Сначала купите компьютер с ОС WinXP и выше");
					}
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Вы не умеете программировать , так что смысла нет !");
				}
			}
		});
		
		button35.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(kurs.getString("kurs", "")) == 1) {
					if (Double.parseDouble(textview2.getText().toString()) > 49999) {
						if ((Double.parseDouble(Seven.getString("Seven", "")) == 1) || (Double.parseDouble(ten.getString("ten", "")) == 1)) {
							button35.setEnabled(false);
							prog.edit().putString("prog", "1").commit();
							programmer = new TimerTask() {
								@Override
								public void run() {
									runOnUiThread(new Runnable() {
										@Override
										public void run() {
											textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + SketchwareUtil.getRandom((int)(10000), (int)(50000)))));
										}
									});
								}
							};
							_timer.scheduleAtFixedRate(programmer, (int)(SketchwareUtil.getRandom((int)(30000), (int)(120000))), (int)(SketchwareUtil.getRandom((int)(30000), (int)(120000))));
						}
						else {
							SketchwareUtil.showMessage(getApplicationContext(), "Сначала купите компьютер с ОС Win7 и выше");
						}
					}
					else {
						SketchwareUtil.showMessage(getApplicationContext(), "Вы не можете устроиться на эту работу!");
					}
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Вы не умеете программировать , так что смысла нет !");
				}
			}
		});
		
		button2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				textview2.setText(String.valueOf((long)(Double.parseDouble("1") + Double.parseDouble(textview2.getText().toString()))));
			}
		});
		
		button27.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				textview2.setText(String.valueOf((long)(Double.parseDouble("2") + Double.parseDouble(textview2.getText().toString()))));
				button27.setEnabled(false);
				musor = new TimerTask() {
					@Override
					public void run() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								button27.setEnabled(true);
							}
						});
					}
				};
				_timer.schedule(musor, (int)(400));
			}
		});
		
		button28.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				button28.setEnabled(false);
				goparick = new TimerTask() {
					@Override
					public void run() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								button28.setEnabled(true);
							}
						});
					}
				};
				_timer.schedule(goparick, (int)(1000));
				if (Double.parseDouble(textview2.getText().toString()) > 500000) {
					_gopnik(SketchwareUtil.getRandom((int)(1), (int)(2)));
				}
				else {
					if (Double.parseDouble(textview2.getText().toString()) > 40000) {
						_gopnik(SketchwareUtil.getRandom((int)(1), (int)(3)));
					}
					else {
						if (Double.parseDouble(textview2.getText().toString()) > 10000) {
							_gopnik(SketchwareUtil.getRandom((int)(1), (int)(5)));
						}
						else {
							if (Double.parseDouble(textview2.getText().toString()) > 5000) {
								_gopnik(SketchwareUtil.getRandom((int)(1), (int)(6)));
							}
							else {
								if (Double.parseDouble(textview2.getText().toString()) > 1000) {
									_gopnik(SketchwareUtil.getRandom((int)(1), (int)(7)));
								}
								else {
									_gopnik(SketchwareUtil.getRandom((int)(1), (int)(9)));
								}
							}
						}
					}
				}
			}
		});
		
		button42.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(kurs.getString("kurs", "")) == 1) {
					button42.setEnabled(false);
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + 400)));
					perw = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									button42.setEnabled(true);
								}
							});
						}
					};
					_timer.schedule(perw, (int)(5000));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Сначала научитесь программировать!");
				}
			}
		});
		
		button43.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(kurs.getString("kurs", "")) == 1) {
					if (((Double.parseDouble(XP.getString("XP", "")) == 1) || (Double.parseDouble(Seven.getString("Seven", "")) == 1)) || (Double.parseDouble(ten.getString("ten", "")) == 1)) {
						button43.setEnabled(false);
						textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + SketchwareUtil.getRandom((int)(0), (int)(1600)))));
						cuapp = new TimerTask() {
							@Override
							public void run() {
								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										button43.setEnabled(true);
									}
								});
							}
						};
						_timer.schedule(cuapp, (int)(SketchwareUtil.getRandom((int)(10000), (int)(30000))));
					}
					else {
						SketchwareUtil.showMessage(getApplicationContext(), "Сначала купите компьютер с ОС WinXP и выше");
					}
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Сначала научитесь программировать!");
				}
			}
		});
		
		button5.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				golod0.setText(String.valueOf((long)(Double.parseDouble(golod0.getText().toString()) + 5)));
				button5.setEnabled(false);
				snf = new TimerTask() {
					@Override
					public void run() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								button5.setEnabled(true);
							}
						});
					}
				};
				_timer.schedule(snf, (int)(1500));
			}
		});
		
		button6.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 20) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 21)));
					golod0.setText(String.valueOf((long)(Double.parseDouble(golod0.getText().toString()) + 12)));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button7.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 69) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 70)));
					golod0.setText(String.valueOf((long)(Double.parseDouble(golod0.getText().toString()) + 42)));
					swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - 6)));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button14.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 4999) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 5000)));
					golod0.setText(String.valueOf((long)(Double.parseDouble(golod0.getText().toString()) + 3250)));
					swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - 1)));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button15.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 24999) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 25000)));
					golod0.setText(String.valueOf((long)(Double.parseDouble(golod0.getText().toString()) + 10000)));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button45.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 999999) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 1000000)));
					golod0.setText(String.valueOf((long)(Double.parseDouble(golod0.getText().toString()) + 500000)));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button29.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 499) {
					free = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - Double.parseDouble("500"))));
									swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) + Double.parseDouble("10"))));
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(free, (int)(30000), (int)(30000));
					frees.edit().putString("freee", "1").commit();
					button29.setEnabled(false);
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button30.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 4999) {
					free = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - Double.parseDouble("5000"))));
									swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) + Double.parseDouble("35"))));
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(free, (int)(30000), (int)(30000));
					sred.edit().putString("normal", "1").commit();
					button30.setEnabled(false);
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button31.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 19999) {
					free = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - Double.parseDouble("10000"))));
									swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) + Double.parseDouble("50"))));
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(free, (int)(30000), (int)(30000));
					full.edit().putString("fuller", "1").commit();
					button31.setEnabled(false);
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button9.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) + 10)));
				button9.setEnabled(false);
				snf = new TimerTask() {
					@Override
					public void run() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								button9.setEnabled(true);
							}
						});
					}
				};
				_timer.schedule(snf, (int)(7000));
			}
		});
		
		button59.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 14) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 15)));
					swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) + 7)));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button58.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 49) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 50)));
					swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) + 15)));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button46.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 999) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 1000)));
					swither0.setText(String.valueOf((long)(99)));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button47.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(Android.getString("Android", "")) == 1) {
					button47.setEnabled(false);
					swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) + SketchwareUtil.getRandom((int)(1), (int)(60)))));
					andro = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									button47.setEnabled(true);
								}
							});
						}
					};
					_timer.schedule(andro, (int)(2500));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Сначала купите телефон с ОС Android !");
				}
			}
		});
		
		button10.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 4999) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 5000)));
					swither0.setText(String.valueOf((long)(100)));
					slowswitz.edit().putString("ss", "1768").commit();
					ss.cancel();
					ss = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - 1)));
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(ss, (int)(Double.parseDouble(slowswitz.getString("ss", ""))), (int)(Double.parseDouble(slowswitz.getString("ss", ""))));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button12.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 49999) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 50000)));
					swither0.setText(String.valueOf((long)(100)));
					slowswitz.edit().putString("ss", "4000").commit();
					ss.cancel();
					ss = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - 1)));
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(ss, (int)(Double.parseDouble(slowswitz.getString("ss", ""))), (int)(Double.parseDouble(slowswitz.getString("ss", ""))));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button13.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 999999) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 1000000)));
					swither0.setText(String.valueOf((long)(100)));
					slowswitz.edit().putString("ss", "10000").commit();
					ss.cancel();
					ss = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - 1)));
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(ss, (int)(Double.parseDouble(slowswitz.getString("ss", ""))), (int)(Double.parseDouble(slowswitz.getString("ss", ""))));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button40.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 9999) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - Double.parseDouble("10000"))));
					Android.edit().putString("Android", "1").commit();
					button40.setEnabled(false);
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button37.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (((Double.parseDouble(frees.getString("freee", "")) == 1) || (Double.parseDouble(sred.getString("normal", "")) == 1)) || (Double.parseDouble(full.getString("fuller", "")) == 1)) {
					if (Double.parseDouble(textview2.getText().toString()) > 19999) {
						textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - Double.parseDouble("20000"))));
						XP.edit().putString("XP", "1").commit();
						button37.setEnabled(false);
					}
					else {
						SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
					}
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Сначала купите дом!");
				}
			}
		});
		
		button38.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (((Double.parseDouble(frees.getString("freee", "")) == 1) || (Double.parseDouble(sred.getString("normal", "")) == 1)) || (Double.parseDouble(full.getString("fuller", "")) == 1)) {
					if (Double.parseDouble(textview2.getText().toString()) > 49999) {
						textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - Double.parseDouble("50000"))));
						Seven.edit().putString("Seven", "1").commit();
						button38.setEnabled(false);
					}
					else {
						SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
					}
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Сначала купите дом!");
				}
			}
		});
		
		button39.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (((Double.parseDouble(frees.getString("freee", "")) == 1) || (Double.parseDouble(sred.getString("normal", "")) == 1)) || (Double.parseDouble(full.getString("fuller", "")) == 1)) {
					if (Double.parseDouble(textview2.getText().toString()) > 199999) {
						textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - Double.parseDouble("200000"))));
						ten.edit().putString("ten", "1").commit();
						button39.setEnabled(false);
					}
					else {
						SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
					}
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Сначала купите дом!");
				}
			}
		});
		
		linear5.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				
			}
		});
		
		uchvschool.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				allgamefile.edit().putString("uch", "1").commit();
				allgamefile.edit().putString("sl", "1").commit();
				allgamefile.edit().putString("ssl", "0").commit();
				allgamefile.edit().putString("sssl", "0").commit();
				textview9.setText("0");
				uchvschool.setEnabled(false);
				button50.setEnabled(false);
				button51.setEnabled(false);
				s1 = new TimerTask() {
					@Override
					public void run() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (Double.parseDouble(textview9.getText().toString()) > 364) {
									s1.cancel();
									allgamefile.edit().putString("sl", "0").commit();
									allgamefile.edit().putString("ssl", "0").commit();
									allgamefile.edit().putString("sssl", "0").commit();
									allgamefile.edit().putString("school", "1").commit();
									uchvschool.setEnabled(true);
									button50.setEnabled(true);
									button51.setEnabled(true);
									allgamefile.edit().putString("uch", "0").commit();
									if (allgamefile.getString("school", "").equals("1")) {
										uchvschool.setEnabled(false);
										ege.setText("1");
									}
									if (allgamefile.getString("sschool", "").equals("1")) {
										button50.setEnabled(false);
										ege.setText("10");
									}
									if (allgamefile.getString("ssschool", "").equals("1")) {
										button51.setEnabled(false);
										ege.setText("49");
									}
								}
								else {
									textview9.setText(String.valueOf((long)(Double.parseDouble(textview9.getText().toString()) + 1)));
									allgamefile.edit().putString("uch", textview9.getText().toString()).commit();
								}
							}
						});
					}
				};
				_timer.scheduleAtFixedRate(s1, (int)(1000), (int)(1000));
			}
		});
		
		button50.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 999) {
					allgamefile.edit().putString("uch", "1").commit();
					allgamefile.edit().putString("sl", "0").commit();
					allgamefile.edit().putString("ssl", "1").commit();
					allgamefile.edit().putString("sssl", "0").commit();
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 1000)));
					textview9.setText("0");
					uchvschool.setEnabled(false);
					button50.setEnabled(false);
					button51.setEnabled(false);
					s1 = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									if (Double.parseDouble(textview9.getText().toString()) > 364) {
										s1.cancel();
										allgamefile.edit().putString("sl", "0").commit();
										allgamefile.edit().putString("ssl", "0").commit();
										allgamefile.edit().putString("sssl", "0").commit();
										allgamefile.edit().putString("sschool", "1").commit();
										uchvschool.setEnabled(true);
										button50.setEnabled(true);
										button51.setEnabled(true);
										allgamefile.edit().putString("uch", "0").commit();
										if (allgamefile.getString("school", "").equals("1")) {
											uchvschool.setEnabled(false);
											ege.setText("1");
										}
										if (allgamefile.getString("sschool", "").equals("1")) {
											button50.setEnabled(false);
											ege.setText("10");
										}
										if (allgamefile.getString("ssschool", "").equals("1")) {
											button51.setEnabled(false);
											ege.setText("49");
										}
									}
									else {
										textview9.setText(String.valueOf((long)(Double.parseDouble(textview9.getText().toString()) + 1)));
										allgamefile.edit().putString("uch", textview9.getText().toString()).commit();
									}
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(s1, (int)(1000), (int)(1000));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button51.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 3999) {
					allgamefile.edit().putString("sl", "0").commit();
					allgamefile.edit().putString("ssl", "0").commit();
					allgamefile.edit().putString("sssl", "1").commit();
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 4000)));
					textview9.setText("0");
					uchvschool.setEnabled(false);
					button50.setEnabled(false);
					button51.setEnabled(false);
					allgamefile.edit().putString("uch", "1").commit();
					s1 = new TimerTask() {
						@Override
						public void run() {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									if (Double.parseDouble(textview9.getText().toString()) > 364) {
										s1.cancel();
										allgamefile.edit().putString("sl", "0").commit();
										allgamefile.edit().putString("ssl", "0").commit();
										allgamefile.edit().putString("sssl", "0").commit();
										allgamefile.edit().putString("ssschool", "1").commit();
										uchvschool.setEnabled(true);
										button50.setEnabled(true);
										button51.setEnabled(true);
										allgamefile.edit().putString("uch", "0").commit();
										if (allgamefile.getString("school", "").equals("1")) {
											uchvschool.setEnabled(false);
											ege.setText("1");
										}
										if (allgamefile.getString("sschool", "").equals("1")) {
											button50.setEnabled(false);
											ege.setText("10");
										}
										if (allgamefile.getString("ssschool", "").equals("1")) {
											button51.setEnabled(false);
											ege.setText("49");
										}
									}
									else {
										textview9.setText(String.valueOf((long)(Double.parseDouble(textview9.getText().toString()) + 1)));
										allgamefile.edit().putString("uch", textview9.getText().toString()).commit();
									}
								}
							});
						}
					};
					_timer.scheduleAtFixedRate(s1, (int)(1000), (int)(1000));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button53.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (allgamefile.getString("eges", "").equals("1")) {
					if (Double.parseDouble(textview2.getText().toString()) > 9999) {
						textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 10000)));
						button53.setEnabled(false);
						allgamefile.edit().putString("do", "1").commit();
					}
					else {
						SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
					}
				}
				else {
					if (Double.parseDouble(textview2.getText().toString()) > 49999) {
						textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 50000)));
						allgamefile.edit().putString("do", "1").commit();
						linear4.setVisibility(View.GONE);
						linear5.setVisibility(View.GONE);
						button53.setEnabled(false);
					}
					else {
						SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
					}
				}
			}
		});
		
		button48.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 14999) {
					button48.setEnabled(false);
					kurs.edit().putString("kurs", "1").commit();
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 15000)));
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button52.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(textview2.getText().toString()) > 4999) {
					textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - 5000)));
					if (ege.getText().toString().equals("49")) {
						_pckrandsdach(SketchwareUtil.getRandom((int)(1), (int)(2)));
					}
					else {
						if (ege.getText().toString().equals("10")) {
							_pckrandsdach(SketchwareUtil.getRandom((int)(1), (int)(10)));
						}
						else {
							if (ege.getText().toString().equals("10")) {
								_pckrandsdach(SketchwareUtil.getRandom((int)(1), (int)(10)));
							}
							else {
								SketchwareUtil.showMessage(getApplicationContext(), "За 5к \"спасибо\", но вы не сдали экзамен.");
							}
						}
					}
				}
				else {
					SketchwareUtil.showMessage(getApplicationContext(), "Недостаточно денег!");
				}
			}
		});
		
		button54.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				
			}
		});
		
		button34.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				
			}
		});
		
		button33.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				_zident(SketchwareUtil.getRandom((int)(1), (int)(3)));
			}
		});
		
		button55.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				
			}
		});
		
		button44.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (Double.parseDouble(swither0.getText().toString()) > 100000) {
					q.setClass(getApplicationContext(), GamewinActivity.class);
					startActivity(q);
					win.edit().putString("win", "Ты стал слишком мачо , даже больше Швейцария , поэтому Швейцарий тебе сразу дал корону ...").commit();
					finish();
				}
				else {
					_nasladswithera(SketchwareUtil.getRandom((int)(1), (int)(2)));
				}
			}
		});
		
		button56.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				
			}
		});
	}
	
	private void initializeLogic() {
		if (filegame.getString("new", "").equals("0")) {
			textview2.setText("0");
			swither0.setText("100");
			golod0.setText("100");
			_menuns(1);
		}
		else {
			textview2.setText(filemoney.getString("money", ""));
			golod0.setText(eda.getString("edas", ""));
			swither0.setText(swith.getString("swither", ""));
			_dlyaobnovaprovpust();
			_menuns(2);
		}
		goloden = new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						golod0.setText(String.valueOf((long)(Double.parseDouble(golod0.getText().toString()) - 1)));
						if ((Double.parseDouble(swither0.getText().toString()) == 14) || (Double.parseDouble(golod0.getText().toString()) == 14)) {
							SketchwareUtil.showMessage(getApplicationContext(), "Внимание! У вас мало запасов еды или недостаток настроения!");
							SketchwareUtil.showMessage(getApplicationContext(), "Внимание! У вас мало запасов еды или недостаток настроения!");
							SketchwareUtil.showMessage(getApplicationContext(), "Внимание! У вас мало запасов еды или недостаток настроения!");
						}
					}
				});
			}
		};
		_timer.scheduleAtFixedRate(goloden, (int)(1000), (int)(1000));
		ss = new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - 1)));
					}
				});
			}
		};
		_timer.scheduleAtFixedRate(ss, (int)(Double.parseDouble(slowswitz.getString("ss", ""))), (int)(Double.parseDouble(slowswitz.getString("ss", ""))));
		proverka = new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (Double.parseDouble(golod0.getText().toString()) < 1) {
							q.setClass(getApplicationContext(), GameoverActivity.class);
							startActivity(q);
							proverka.cancel();
							die.edit().putString("die", "От голода").commit();
							finish();
						}
						if (Double.parseDouble(swither0.getText().toString()) < 1) {
							q.setClass(getApplicationContext(), GameoverActivity.class);
							startActivity(q);
							proverka.cancel();
							die.edit().putString("die", "От недостатка настроения").commit();
							finish();
						}
						if (Double.parseDouble(swither0.getText().toString()) > 99) {
							swither0.setText("100");
						}
					}
				});
			}
		};
		_timer.scheduleAtFixedRate(proverka, (int)(0), (int)(1000));
		_startgamefunc();
	}
	
	@Override
	public void onBackPressed() {
		goloden.cancel();
		proverka.cancel();
		filemoney.edit().putString("money", textview2.getText().toString()).commit();
		eda.edit().putString("edas", golod0.getText().toString()).commit();
		swith.edit().putString("swither", swither0.getText().toString()).commit();
		q.setClass(getApplicationContext(), MainActivity.class);
		startActivity(q);
		finish();
	}
	
	@Override
	public void onStop() {
		super.onStop();
		filemoney.edit().putString("money", textview2.getText().toString()).commit();
		eda.edit().putString("edas", golod0.getText().toString()).commit();
		swith.edit().putString("swither", swither0.getText().toString()).commit();
	}
	public void _gopnik(final double _kosar) {
		if (_kosar == 1) {
			SketchwareUtil.showMessage(getApplicationContext(), "Вы не смогли сгопничать косарь");
			swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - SketchwareUtil.getRandom((int)(0), (int)(5)))));
		}
		if (_kosar == 2) {
			SketchwareUtil.showMessage(getApplicationContext(), "Вы смогли сгопничать");
			textview2.setText(String.valueOf((long)(SketchwareUtil.getRandom((int)(1), (int)(1000)) + Double.parseDouble(textview2.getText().toString()))));
		}
		if (_kosar == 3) {
			SketchwareUtil.showMessage(getApplicationContext(), "Вы не смогли сгопничать косарь");
			swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - SketchwareUtil.getRandom((int)(0), (int)(5)))));
		}
		if (_kosar == 4) {
			SketchwareUtil.showMessage(getApplicationContext(), "Вы не смогли сгопничать косарь");
			swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - SketchwareUtil.getRandom((int)(0), (int)(5)))));
		}
		if (_kosar == 5) {
			SketchwareUtil.showMessage(getApplicationContext(), "Вы не смогли сгопничать косарь");
			swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - SketchwareUtil.getRandom((int)(0), (int)(5)))));
		}
		if (_kosar == 6) {
			SketchwareUtil.showMessage(getApplicationContext(), "Вы не смогли сгопничать косарь");
			swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - SketchwareUtil.getRandom((int)(0), (int)(5)))));
		}
		if (_kosar == 7) {
			SketchwareUtil.showMessage(getApplicationContext(), "Вы не смогли сгопничать косарь");
			swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - SketchwareUtil.getRandom((int)(0), (int)(5)))));
		}
		if (_kosar == 8) {
			SketchwareUtil.showMessage(getApplicationContext(), "Вы не смогли сгопничать косарь");
			swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - SketchwareUtil.getRandom((int)(0), (int)(5)))));
		}
		if (_kosar == 9) {
			SketchwareUtil.showMessage(getApplicationContext(), "Вы не смогли сгопничать косарь");
			swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) - SketchwareUtil.getRandom((int)(0), (int)(5)))));
		}
	}
	
	
	public void _zident(final double _pickprez) {
		if (_pickprez == 3) {
			SketchwareUtil.showMessage(getApplicationContext(), "Возможно , но точно не ты");
		}
		if (_pickprez == 2) {
			SketchwareUtil.showMessage(getApplicationContext(), "Вы никогда не станете им");
		}
		if (_pickprez == 1) {
			SketchwareUtil.showMessage(getApplicationContext(), "Ты что , думал что президентом становятся , если много денег?");
		}
	}
	
	
	public void _startgamefunc() {
		if (frees.getString("freee", "").equals("0")) {
			
		}
		else {
			free = new TimerTask() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - Double.parseDouble("500"))));
							swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) + Double.parseDouble("10"))));
						}
					});
				}
			};
			_timer.scheduleAtFixedRate(free, (int)(30000), (int)(30000));
			button29.setEnabled(false);
		}
		if (sred.getString("normal", "").equals("0")) {
			
		}
		else {
			free = new TimerTask() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - Double.parseDouble("5000"))));
							swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) + Double.parseDouble("35"))));
						}
					});
				}
			};
			_timer.scheduleAtFixedRate(free, (int)(30000), (int)(30000));
			button30.setEnabled(false);
		}
		if (full.getString("fuller", "").equals("0")) {
			
		}
		else {
			free = new TimerTask() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) - Double.parseDouble("10000"))));
							swither0.setText(String.valueOf((long)(Double.parseDouble(swither0.getText().toString()) + Double.parseDouble("50"))));
						}
					});
				}
			};
			_timer.scheduleAtFixedRate(free, (int)(30000), (int)(30000));
			button31.setEnabled(false);
		}
		if (dvor.getString("dvor", "").equals("0")) {
			
		}
		else {
			dvornik = new TimerTask() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + 10)));
						}
					});
				}
			};
			_timer.scheduleAtFixedRate(dvornik, (int)(1000), (int)(1000));
			button3.setEnabled(false);
		}
		if (rab.getString("rab", "").equals("0")) {
			
		}
		else {
			rabochiy = new TimerTask() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + 800)));
						}
					});
				}
			};
			_timer.scheduleAtFixedRate(rabochiy, (int)(30000), (int)(30000));
			button4.setEnabled(false);
		}
		if (prod.getString("prod", "").equals("0")) {
			
		}
		else {
			prodavec = new TimerTask() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + 2000)));
						}
					});
				}
			};
			_timer.scheduleAtFixedRate(prodavec, (int)(30000), (int)(30000));
			button17.setEnabled(false);
		}
		if (parik.getString("parik", "").equals("0")) {
			
		}
		else {
			parick = new TimerTask() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + 5000)));
						}
					});
				}
			};
			_timer.scheduleAtFixedRate(parick, (int)(30000), (int)(30000));
			button32.setEnabled(false);
		}
		if (XP.getString("XP", "").equals("0")) {
			
		}
		else {
			button37.setEnabled(false);
		}
		if (Android.getString("Android", "").equals("0")) {
			
		}
		else {
			button40.setEnabled(false);
		}
		if (Seven.getString("Seven", "").equals("0")) {
			
		}
		else {
			button38.setEnabled(false);
		}
		if (ten.getString("ten", "").equals("0")) {
			
		}
		else {
			button39.setEnabled(false);
		}
		if (progdoma.getString("progdoma", "").equals("0")) {
			
		}
		else {
			button36.setEnabled(false);
			progdomas = new TimerTask() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + SketchwareUtil.getRandom((int)(0), (int)(5500)))));
						}
					});
				}
			};
			_timer.scheduleAtFixedRate(progdomas, (int)(30000), (int)(30000));
		}
		if (prog.getString("prog", "").equals("0")) {
			
		}
		else {
			button35.setEnabled(false);
			programmer = new TimerTask() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							textview2.setText(String.valueOf((long)(Double.parseDouble(textview2.getText().toString()) + SketchwareUtil.getRandom((int)(10000), (int)(50000)))));
						}
					});
				}
			};
			_timer.scheduleAtFixedRate(programmer, (int)(SketchwareUtil.getRandom((int)(30000), (int)(120000))), (int)(SketchwareUtil.getRandom((int)(30000), (int)(120000))));
		}
		if (kurs.getString("kurs", "").equals("0")) {
			
		}
		else {
			button48.setEnabled(false);
		}
		if (allgamefile.getString("school", "").equals("1")) {
			uchvschool.setEnabled(false);
			ege.setText("1");
		}
		if (allgamefile.getString("sschool", "").equals("1")) {
			button50.setEnabled(false);
			ege.setText("10");
		}
		if (allgamefile.getString("ssschool", "").equals("1")) {
			button51.setEnabled(false);
			ege.setText("49");
		}
		if (allgamefile.getString("eges", "").equals("1")) {
			linear4.setVisibility(View.GONE);
			linear5.setVisibility(View.GONE);
			button53.setText("Дальнейшая учёба - 10 000");
		}
		if (allgamefile.getString("do", "").equals("1")) {
			linear4.setVisibility(View.GONE);
			linear5.setVisibility(View.GONE);
			button53.setEnabled(false);
		}
		if (allgamefile.getString("uch", "").equals("0")) {
			
		}
		else {
			textview9.setText(allgamefile.getString("uch", ""));
			uchvschool.setEnabled(false);
			button50.setEnabled(false);
			button51.setEnabled(false);
			s1 = new TimerTask() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (Double.parseDouble(textview9.getText().toString()) > 364) {
								s1.cancel();
								if (allgamefile.getString("sl", "").equals("1")) {
									allgamefile.edit().putString("school", "1").commit();
									allgamefile.edit().putString("sl", "0").commit();
									allgamefile.edit().putString("ssl", "0").commit();
									allgamefile.edit().putString("sssl", "0").commit();
								}
								if (allgamefile.getString("ssl", "").equals("1")) {
									allgamefile.edit().putString("sschool", "1").commit();
									allgamefile.edit().putString("sl", "0").commit();
									allgamefile.edit().putString("ssl", "0").commit();
									allgamefile.edit().putString("sssl", "0").commit();
								}
								if (allgamefile.getString("sssl", "").equals("1")) {
									allgamefile.edit().putString("ssschool", "1").commit();
									allgamefile.edit().putString("sl", "0").commit();
									allgamefile.edit().putString("ssl", "0").commit();
									allgamefile.edit().putString("sssl", "0").commit();
								}
								uchvschool.setEnabled(true);
								button50.setEnabled(true);
								button51.setEnabled(true);
								allgamefile.edit().putString("uch", "0").commit();
								if (allgamefile.getString("school", "").equals("1")) {
									uchvschool.setEnabled(false);
									ege.setText("1");
								}
								if (allgamefile.getString("sschool", "").equals("1")) {
									button50.setEnabled(false);
									ege.setText("10");
								}
								if (allgamefile.getString("ssschool", "").equals("1")) {
									button51.setEnabled(false);
									ege.setText("49");
								}
							}
							else {
								textview9.setText(String.valueOf((long)(Double.parseDouble(textview9.getText().toString()) + 1)));
								allgamefile.edit().putString("uch", textview9.getText().toString()).commit();
							}
						}
					});
				}
			};
			_timer.scheduleAtFixedRate(s1, (int)(1000), (int)(1000));
		}
	}
	
	
	public void _nasladswithera(final double _swithers) {
		if (_swithers == 1) {
			SketchwareUtil.showMessage(getApplicationContext(), "Стать швейцарием можно , только нужно наслаждаться швейцарками каждый день...");
		}
		else {
			SketchwareUtil.showMessage(getApplicationContext(), "Король Швейцарий тебя не принял в короли . Он сказал : Ты не на 101%> мачо");
		}
	}
	
	
	public void _dlyaobnovaprovpust() {
		if (allgamefile.getString("school", "").equals("")) {
			allgamefile.edit().putString("school", "0").commit();
		}
		if (allgamefile.getString("sschool", "").equals("")) {
			allgamefile.edit().putString("sschool", "0").commit();
		}
		if (allgamefile.getString("ssschool", "").equals("")) {
			allgamefile.edit().putString("ssschool", "0").commit();
		}
		if (allgamefile.getString("sl", "").equals("")) {
			allgamefile.edit().putString("sl", "0").commit();
		}
		if (allgamefile.getString("ssl", "").equals("")) {
			allgamefile.edit().putString("ssl", "0").commit();
		}
		if (allgamefile.getString("sssl", "").equals("")) {
			allgamefile.edit().putString("sssl", "0").commit();
		}
		if (allgamefile.getString("do", "").equals("")) {
			allgamefile.edit().putString("do", "0").commit();
		}
		if (allgamefile.getString("uch", "").equals("")) {
			allgamefile.edit().putString("uch", "0").commit();
		}
		if (allgamefile.getString("eges", "").equals("")) {
			allgamefile.edit().putString("eges", "0").commit();
		}
		if (slowswitz.getString("ss", "").equals("")) {
			slowswitz.edit().putString("ss", "1000").commit();
		}
		if (frees.getString("freee", "").equals("")) {
			frees.edit().putString("freee", "0").commit();
		}
		if (sred.getString("normal", "").equals("")) {
			sred.edit().putString("normal", "0").commit();
		}
		if (full.getString("fuller", "").equals("")) {
			full.edit().putString("fuller", "0").commit();
		}
		if (XP.getString("XP", "").equals("")) {
			XP.edit().putString("XP", "0").commit();
		}
		if (Android.getString("Android", "").equals("")) {
			Android.edit().putString("Android", "0").commit();
		}
		if (Seven.getString("Seven", "").equals("")) {
			Seven.edit().putString("Seven", "0").commit();
		}
		if (ten.getString("ten", "").equals("")) {
			ten.edit().putString("ten", "0").commit();
		}
		if (progdoma.getString("progdoma", "").equals("")) {
			progdoma.edit().putString("progdoma", "0").commit();
		}
		if (prog.getString("prog", "").equals("")) {
			prog.edit().putString("prog", "0").commit();
		}
		if (kurs.getString("kurs", "").equals("")) {
			kurs.edit().putString("kurs", "0").commit();
		}
	}
	
	
	public void _menuns(final double _vkladka) {
		if (_vkladka == 1) {
			rabota.setVisibility(View.GONE);
			zrachka.setVisibility(View.GONE);
			naslodi.setVisibility(View.GONE);
			house.setVisibility(View.GONE);
			os.setVisibility(View.GONE);
			dop.setVisibility(View.GONE);
			mechtafuture.setVisibility(View.GONE);
			doprabstvo.setBackgroundColor(0xFF009688);
		}
		if (_vkladka == 2) {
			doprabota.setVisibility(View.GONE);
			zrachka.setVisibility(View.GONE);
			naslodi.setVisibility(View.GONE);
			house.setVisibility(View.GONE);
			os.setVisibility(View.GONE);
			dop.setVisibility(View.GONE);
			mechtafuture.setVisibility(View.GONE);
			rabstvo.setBackgroundColor(0xFF009688);
		}
	}
	
	
	public void _pckrandsdach(final double _randomiznoesege) {
		if (_randomiznoesege == 1) {
			SketchwareUtil.showMessage(getApplicationContext(), "Поздравляю вы сдали экзамен!");
			allgamefile.edit().putString("eges", "1").commit();
			linear4.setVisibility(View.GONE);
			linear5.setVisibility(View.GONE);
			button53.setText("Дальнейшая учёба - 10 000");
		}
		else {
			SketchwareUtil.showMessage(getApplicationContext(), "Увы, вы не сдали экзамен.");
		}
	}
	
	
	@Deprecated
	public void showMessage(String _s) {
		Toast.makeText(getApplicationContext(), _s, Toast.LENGTH_SHORT).show();
	}
	
	@Deprecated
	public int getLocationX(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[0];
	}
	
	@Deprecated
	public int getLocationY(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[1];
	}
	
	@Deprecated
	public int getRandom(int _min, int _max) {
		Random random = new Random();
		return random.nextInt(_max - _min + 1) + _min;
	}
	
	@Deprecated
	public ArrayList<Double> getCheckedItemPositionsToArray(ListView _list) {
		ArrayList<Double> _result = new ArrayList<Double>();
		SparseBooleanArray _arr = _list.getCheckedItemPositions();
		for (int _iIdx = 0; _iIdx < _arr.size(); _iIdx++) {
			if (_arr.valueAt(_iIdx))
			_result.add((double)_arr.keyAt(_iIdx));
		}
		return _result;
	}
	
	@Deprecated
	public float getDip(int _input) {
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, _input, getResources().getDisplayMetrics());
	}
	
	@Deprecated
	public int getDisplayWidthPixels() {
		return getResources().getDisplayMetrics().widthPixels;
	}
	
	@Deprecated
	public int getDisplayHeightPixels() {
		return getResources().getDisplayMetrics().heightPixels;
	}
}