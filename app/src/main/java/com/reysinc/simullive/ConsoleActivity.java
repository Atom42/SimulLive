package com.reysinc.simullive;

import android.animation.*;
import android.app.*;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.*;
import android.content.SharedPreferences;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.media.*;
import android.net.*;
import android.os.*;
import android.text.*;
import android.text.style.*;
import android.util.*;
import android.view.*;
import android.view.View;
import android.view.View.*;
import android.view.animation.*;
import android.webkit.*;
import android.widget.*;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;
import org.json.*;

public class ConsoleActivity extends Activity {
	
	private LinearLayout linear6;
	private ScrollView vscroll1;
	private CheckBox checkbox1;
	private LinearLayout linear1;
	private TextView textview1;
	private LinearLayout linear2;
	private TextView textview2;
	private LinearLayout linear3;
	private TextView textview3;
	private LinearLayout linear4;
	private TextView textview4;
	private LinearLayout linear7;
	private Button button1;
	private EditText edittext1;
	private Button button2;
	private EditText edittext2;
	private Button button3;
	private EditText edittext3;
	private Button button4;
	private EditText hzotnimtime;
	
	private SharedPreferences filemoney;
	private SharedPreferences eda;
	private SharedPreferences swith;
	private SharedPreferences VIP;
	private SharedPreferences slowswitz;
	
	@Override
	protected void onCreate(Bundle _savedInstanceState) {
		super.onCreate(_savedInstanceState);
		setContentView(R.layout.console);
		initialize(_savedInstanceState);
		initializeLogic();
	}
	
	private void initialize(Bundle _savedInstanceState) {
		linear6 = findViewById(R.id.linear6);
		vscroll1 = findViewById(R.id.vscroll1);
		checkbox1 = findViewById(R.id.checkbox1);
		linear1 = findViewById(R.id.linear1);
		textview1 = findViewById(R.id.textview1);
		linear2 = findViewById(R.id.linear2);
		textview2 = findViewById(R.id.textview2);
		linear3 = findViewById(R.id.linear3);
		textview3 = findViewById(R.id.textview3);
		linear4 = findViewById(R.id.linear4);
		textview4 = findViewById(R.id.textview4);
		linear7 = findViewById(R.id.linear7);
		button1 = findViewById(R.id.button1);
		edittext1 = findViewById(R.id.edittext1);
		button2 = findViewById(R.id.button2);
		edittext2 = findViewById(R.id.edittext2);
		button3 = findViewById(R.id.button3);
		edittext3 = findViewById(R.id.edittext3);
		button4 = findViewById(R.id.button4);
		hzotnimtime = findViewById(R.id.hzotnimtime);
		filemoney = getSharedPreferences("filemoney", Activity.MODE_PRIVATE);
		eda = getSharedPreferences("eda", Activity.MODE_PRIVATE);
		swith = getSharedPreferences("swith", Activity.MODE_PRIVATE);
		VIP = getSharedPreferences("VIPMODE", Activity.MODE_PRIVATE);
		slowswitz = getSharedPreferences("slowswitz", Activity.MODE_PRIVATE);
		
		checkbox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton _param1, boolean _param2) {
				final boolean _isChecked = _param2;
				if (_isChecked) {
					VIP.edit().putString("VIP", "true").commit();
				}
				else {
					VIP.edit().putString("VIP", "false").commit();
				}
			}
		});
		
		button1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (edittext1.getText().toString().equals("")) {
					SketchwareUtil.showMessage(getApplicationContext(), "Ошибка! Пустое значение.");
				}
				else {
					filemoney.edit().putString("money", edittext1.getText().toString()).commit();
					SketchwareUtil.showMessage(getApplicationContext(), "Выполнено успешно!");
				}
			}
		});
		
		button2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (edittext2.getText().toString().equals("")) {
					SketchwareUtil.showMessage(getApplicationContext(), "Ошибка! Пустое значение.");
				}
				else {
					eda.edit().putString("edas", edittext2.getText().toString()).commit();
					SketchwareUtil.showMessage(getApplicationContext(), "Выполнено успешно!");
				}
			}
		});
		
		button3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (edittext3.getText().toString().equals("")) {
					SketchwareUtil.showMessage(getApplicationContext(), "Ошибка! Пустое значение.");
				}
				else {
					swith.edit().putString("swither", edittext3.getText().toString()).commit();
					SketchwareUtil.showMessage(getApplicationContext(), "Выполнено успешно!");
				}
			}
		});
		
		button4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				if (hzotnimtime.getText().toString().equals("")) {
					SketchwareUtil.showMessage(getApplicationContext(), "Ошибка! Пустое значение.");
				}
				else {
					slowswitz.edit().putString("ss", hzotnimtime.getText().toString()).commit();
					SketchwareUtil.showMessage(getApplicationContext(), "Выполнено успешно!");
				}
			}
		});
	}
	
	private void initializeLogic() {
		if (VIP.getString("VIP", "").equals("true")) {
			checkbox1.setChecked(true);
		}
		else {
			checkbox1.setChecked(false);
		}
	}
	
	
	@Deprecated
	public void showMessage(String _s) {
		Toast.makeText(getApplicationContext(), _s, Toast.LENGTH_SHORT).show();
	}
	
	@Deprecated
	public int getLocationX(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[0];
	}
	
	@Deprecated
	public int getLocationY(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[1];
	}
	
	@Deprecated
	public int getRandom(int _min, int _max) {
		Random random = new Random();
		return random.nextInt(_max - _min + 1) + _min;
	}
	
	@Deprecated
	public ArrayList<Double> getCheckedItemPositionsToArray(ListView _list) {
		ArrayList<Double> _result = new ArrayList<Double>();
		SparseBooleanArray _arr = _list.getCheckedItemPositions();
		for (int _iIdx = 0; _iIdx < _arr.size(); _iIdx++) {
			if (_arr.valueAt(_iIdx))
			_result.add((double)_arr.keyAt(_iIdx));
		}
		return _result;
	}
	
	@Deprecated
	public float getDip(int _input) {
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, _input, getResources().getDisplayMetrics());
	}
	
	@Deprecated
	public int getDisplayWidthPixels() {
		return getResources().getDisplayMetrics().widthPixels;
	}
	
	@Deprecated
	public int getDisplayHeightPixels() {
		return getResources().getDisplayMetrics().heightPixels;
	}
}